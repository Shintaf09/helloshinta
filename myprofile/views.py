from django.shortcuts import render

# Create your views here.
def index(request):
	return render(request, "index.html")

def aboutMe(request):
	return render(request, "aboutme.html")

def gallery(request):
	return render(request, "gallery.html")