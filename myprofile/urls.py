from .views import index, aboutMe, gallery
from django.urls import path

urlpatterns = [
	path('', index, name="index"),
	path('aboutme/', aboutMe, name="aboutMe"),
	path('gallery/', gallery, name="gallery"),
]